# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [Unreleased]

## [0.1.0] - 2021-01-15

Initial release


[Unreleased]: https://codeberg.org/adot/telemachus/compare/v0.1.0...trunk
[0.1.0]: https://codeberg.org/adot/telemachus/src/tag/v0.1.0