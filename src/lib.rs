use std::borrow::Cow;

use serde::{Deserialize, Serialize};
use smallvec::SmallVec;

mod parse;
mod render;

#[derive(Serialize, Deserialize)]
pub struct Template<'t> {
    parts: Vec<Block<'t>>,
}

#[derive(Serialize, Deserialize)]
enum Block<'t> {
    HtmlLit(Cow<'t, str>),
    PutUnescaped(Selector<'t>),
    PutEscaped(Selector<'t>),
    If {
        sel: Selector<'t>,
        if_true: Vec<Block<'t>>,
        if_false: Vec<Block<'t>>,
    },
    For {
        /// What to name the thing. Eg `i`
        name: Cow<'t, str>,
        iter_over: Selector<'t>,
        each: Vec<Block<'t>>,
    },
    Comment,
}

#[derive(Debug, PartialEq, Serialize, Deserialize, Default)]
struct Selector<'t> {
    items: SmallVec<[SelPart<'t>; 1]>,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
enum SelPart<'t> {
    Map(Cow<'t, str>),
    Array(usize),
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
